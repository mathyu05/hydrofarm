package farmeroo.hydrofarm;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.List;

import farmeroo.hydrofarm.db.AppDatabase;
import farmeroo.hydrofarm.db.dao.FarmDao;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Created by mathy on 24/01/2018.
 */

@RunWith(AndroidJUnit4.class)
public class DatabaseTest {
    private FarmDao mFarmDao;
    private AppDatabase mDB;

    @Before
    public void createDB() {
        Context context = InstrumentationRegistry.getTargetContext();
        mDB = Room.inMemoryDatabaseBuilder(context, AppDatabase.class).build();
        mFarmDao = mDB.getFarmDao();
    }

    @After
    public void closeDB() throws IOException {
        mDB.close();
    }

    @Test
    public void writeUserAndReadInList() throws Exception {
        // Arrange
        Farm farm = TestUtil.createFarm();
        farm.setFarmID(1);
        farm.setFarmName("Lockrose");
        mFarmDao.insertAll(farm);

        // Act
        List<Farm> byName = mFarmDao.findFarmsByName("Lockrose");

        // Assert
        assertThat(byName.get(0), equalTo(farm));
    }
}