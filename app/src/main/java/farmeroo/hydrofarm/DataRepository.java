package farmeroo.hydrofarm;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;

import java.util.List;

import farmeroo.hydrofarm.db.source.local.AppDatabase;
import farmeroo.hydrofarm.db.source.local.entities.FarmEntity;

/**
 * Repository handling the work with products and comments.
 */
public class DataRepository {

    private static DataRepository sInstance;

    private final AppDatabase mDatabase;
    private MediatorLiveData<List<FarmEntity>> mObservableFarms;

    private DataRepository(final AppDatabase database) {
        mDatabase = database;
        mObservableFarms = new MediatorLiveData<>();

        mObservableFarms.addSource(mDatabase.farmDao().getAll(),
                farmEntities -> {
                    if (mDatabase.getDatabaseCreated().getValue() != null) {
                        mObservableFarms.postValue(farmEntities);
                    }
                });
    }

    public static DataRepository getInstance(final AppDatabase database) {
        if (sInstance == null) {
            synchronized (DataRepository.class) {
                if (sInstance == null) {
                    sInstance = new DataRepository(database);
                }
            }
        }
        return sInstance;
    }

    /**
     * Get the list of products from the database and get notified when the data changes.
     */
    public LiveData<List<FarmEntity>> getFarms() {
        return mObservableFarms;
    }

    public LiveData<FarmEntity> loadFarm(final int farmID) {
        return mDatabase.farmDao().findFarmByID(farmID);
    }
}
