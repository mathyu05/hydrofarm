package farmeroo.hydrofarm.db.source.local.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import farmeroo.hydrofarm.db.source.local.entities.FarmEntity;

/**
 * Created by mathy on 23/01/2018.
 */

@Dao
public interface FarmDao {

    @Query("SELECT * FROM farms")
    LiveData<List<FarmEntity>> getAll();

    @Query("SELECT * FROM farms WHERE farms.farmName = :farmName")
    LiveData<FarmEntity> findFarmByName(String farmName);

    @Query("SELECT * FROM farms WHERE farms.farmID = :farmID")
    LiveData<FarmEntity> findFarmByID(int farmID);

    @Insert
    void insertAll(FarmEntity... farms);

    @Update
    void updateUsers(FarmEntity... farms);

    @Delete
    void delete(FarmEntity farm);
}
