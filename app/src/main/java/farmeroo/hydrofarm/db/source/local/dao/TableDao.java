package farmeroo.hydrofarm.db.source.local.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import farmeroo.hydrofarm.db.source.local.entities.TableEntity;

/**
 * Created by mathy on 24/01/2018.
 */

@Dao
public interface TableDao {

    @Query("SELECT * FROM tables")
    LiveData<List<TableEntity>> getAll();

    @Insert
    void insertAll(TableEntity... table);

    @Update
    void updateUsers(TableEntity... table);

    @Delete
    void delete(TableEntity table);
}
