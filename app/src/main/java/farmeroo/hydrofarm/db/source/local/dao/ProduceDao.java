package farmeroo.hydrofarm.db.source.local.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import farmeroo.hydrofarm.db.source.local.entities.ProduceEntity;

/**
 * Created by mathy on 24/01/2018.
 */

@Dao
public interface ProduceDao {

    @Query("SELECT * FROM produce")
    LiveData<List<ProduceEntity>> getAll();

    @Insert(onConflict = OnConflictStrategy.FAIL)
    void insertAll(ProduceEntity... produceEntity);

    @Update
    void updateUsers(ProduceEntity... produceEntity);

    @Delete
    void delete(ProduceEntity produceEntity);
}
