package farmeroo.hydrofarm.db.source.local.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import farmeroo.hydrofarm.db.source.local.entities.TableEntity;

/**
 * Created by mathy on 24/01/2018.
 */

@Entity(tableName = "produce",
        indices = {@Index(value = "produceID", unique = true),
                    @Index(value = "tableID", unique = true)},
        foreignKeys = @ForeignKey(entity = TableEntity.class,
                        parentColumns = "tableID",
                        childColumns = "tableID"))
public class ProduceEntity {

    @PrimaryKey(autoGenerate = true)
    private int produceID;

    private String produceName;

    private int tableID;


    // Constructors

    public ProduceEntity() {}

    public ProduceEntity(int produceID, String produceName, int tableID) {

        this.produceID = produceID;
        this.produceName = produceName;
        this.tableID = tableID;
    }

    public ProduceEntity(ProduceEntity produce) {
        this.produceID = produce.getProduceID();
        this.produceName = produce.getProduceName();
        this.tableID = produce.getTableID();
    }


    // Getters & Setters

    public int getProduceID() {
        return produceID;
    }

    public void setProduceID(int produceID) {
        this.produceID = produceID;
    }

    public String getProduceName() {
        return produceName;
    }

    public void setProduceName(String produceName) {
        this.produceName = produceName;
    }

    public int getTableID() {
        return tableID;
    }

    public void setTableID(int tableID) {
        this.tableID = tableID;
    }
}