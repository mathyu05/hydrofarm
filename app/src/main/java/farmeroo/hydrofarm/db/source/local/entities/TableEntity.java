package farmeroo.hydrofarm.db.source.local.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by mathy on 24/01/2018.
 */
@Entity(tableName = "tables",
        primaryKeys = "tableID",
        foreignKeys = @ForeignKey(entity = FarmEntity.class,
                        parentColumns = "farmID",
                        childColumns = "farmID"),
        indices = {@Index(value = "tableID", unique = true),
                    @Index(value = "farmID")})
public class TableEntity {

    @PrimaryKey(autoGenerate = true)
    private int tableID;

    private int farmID;


    // Constructors

    public TableEntity() {}

    public TableEntity(int tableID, int farmID) {
        this.tableID = tableID;
        this.farmID = farmID;
    }

    public TableEntity(TableEntity table) {
        this.tableID = table.getTableID();
        this.farmID = table.getFarmID();
    }


    // Getters & Setters

    public int getTableID() {
        return tableID;
    }

    public void setTableID(int tableID) {
        this.tableID = tableID;
    }

    public int getFarmID() {
        return farmID;
    }

    public void setFarmID(int farmID) {
        this.farmID = farmID;
    }
}