package farmeroo.hydrofarm.db.source.local.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by mathy on 23/01/2018.
 */

@Entity(tableName = "farms",
        primaryKeys = "farmID",
        indices = {@Index(value = "farmName", unique = true)})
public class FarmEntity {

    @PrimaryKey(autoGenerate = true)
    private int farmID;

    private String farmName;


    // Constructors

    public FarmEntity() {}

    public FarmEntity(int farmID, String farmName) {
        this.farmID = farmID;
        this.farmName = farmName;
    }

    public FarmEntity(FarmEntity farm) {
        this.farmID = farm.getFarmID();
        this.farmName = farm.getFarmName();
    }


    // Getters & Setters

    public int getFarmID() {
        return farmID;
    }

    public void setFarmID(int farmID) {
        this.farmID = farmID;
    }

    public String getFarmName() {
        return farmName;
    }

    public void setFarmName(String farmName) {
        this.farmName = farmName;
    }
}